# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table local (
  id                        bigserial not null,
  name                      varchar(255),
  cnpj                      varchar(255),
  type                      integer,
  user_id                   bigint,
  key                       varchar(255),
  last_update               timestamp not null,
  constraint ck_local_type check (type in (0,1)),
  constraint pk_local primary key (id))
;

create table main_user (
  id                        bigserial not null,
  name                      varchar(255),
  cpf                       varchar(255),
  id_gcm_user               varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  last_update               timestamp not null,
  constraint pk_main_user primary key (id))
;

create table task (
  id                        bigserial not null,
  label                     varchar(255),
  constraint pk_task primary key (id))
;

alter table local add constraint fk_local_mainUser_1 foreign key (user_id) references main_user (id);
create index ix_local_mainUser_1 on local (user_id);



# --- !Downs

drop table if exists local cascade;

drop table if exists main_user cascade;

drop table if exists task cascade;

