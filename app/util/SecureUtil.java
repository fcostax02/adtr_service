package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SecureUtil {

	public static String encryptPassword(String password)
			throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA1");
		BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
		String s = hash.toString(16);
		if (s.length() % 2 != 0) {
			s = "0" + s;
		}
		return s;
	}

	public static String generateNumberRandom() {
		Random random = new Random();
		return "" + random.nextInt(1000000);
	}
}
