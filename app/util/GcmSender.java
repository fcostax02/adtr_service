package util;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import play.Logger;

import java.util.List;

/**
 * Created by lacroiix on 10/11/15.
 */
public class GcmSender {

    public static void send(List<String> to, String data) {

        final String API_KEY = "AIzaSyA4RfBF9h4O7-oV9cmp5qPiveH99p2BV4I";

        try {
            String REQUEST_URL = "https://android.googleapis.com/gcm/send";

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(REQUEST_URL);

            httpPost.setHeader("Authorization", "key=" + API_KEY);
            httpPost.setHeader("Content-Type", "application/json");


            StringEntity params = new StringEntity(
                    "{ \"data\": {\"message\": " + data + "}"
                            + "\"registration_ids\": [\"" + to.get(0) + "\"]}");

            httpPost.setEntity(params);

            int i = httpclient.execute(httpPost).getStatusLine().getStatusCode();

            System.out.println("--- Code: " + i);


        } catch (Exception e) {
            Logger.error(e.toString());
        }
    }
}
