package models;

import javax.persistence.*;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.validator.constraints.br.CNPJ;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Entity
public class Local extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Constraints.Required
    private String name;
    private String cnpj;
    @Constraints.Required
    private LocalType type;
    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private MainUser mainUser;
    @Version
    private Timestamp lastUpdate;
    private String key;

    public Local() {
    }

	/*
    CRUD
     */

    public static Finder<Long, Local> find = new Finder(
            Long.class, Local.class
    );

    public static List<Local> all() {
        return find.all();
    }

    public static void create(Local local) {
        local.save();
    }

    public static void delete(Long id) {
        find.ref(id).delete();
    }

    /*
    Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public LocalType getType() {
        return type;
    }

    public void setType(LocalType type) {
        this.type = type;
    }

    public MainUser getMainUser() {
        return mainUser;
    }

    public void setMainUser(MainUser mainUser) {
        this.mainUser = mainUser;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
