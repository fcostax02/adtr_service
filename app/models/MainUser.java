package models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.br.CPF;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by lacroiix on 02/10/15.
 */
@Entity
public class MainUser extends Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Constraints.Required
    private String name;
    private String cpf;
    private String idGcmUser;
    @Constraints.Email
    private String email;
    private String password;
    @OneToMany(mappedBy = "mainUser", fetch=FetchType.EAGER)
    @JsonManagedReference
    private List<Local> locals;
    @Version
    public Timestamp lastUpdate;

    public MainUser() {
    }

    public MainUser(Long id, String name, String cpf, String email) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.email = email;
    }

    public static MainUser getDefaultUser() {
        return new MainUser(999666l, "Usuário nao identificado", "00000000000", "email não " +
                "identificado");
    }

    /*
    CRUD
     */

    public static Finder<Long,MainUser> find = new Finder(
            Long.class, MainUser.class
    );

    public static List<MainUser> all() {
        return find.all();
    }

    public static void create(MainUser user) {
        user.save();
    }

    /*
    Getters and Setters
     */

    public static void delete(Long id) {
        find.ref(id).delete();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdGcmUser() {
        return idGcmUser;
    }

    public void setIdGcmUser(String idGcmUser) {
        this.idGcmUser = idGcmUser;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Local> getLocals() {
        return locals;
    }

    public void setLocals(List<Local> locals) {
        this.locals = locals;
    }
}
