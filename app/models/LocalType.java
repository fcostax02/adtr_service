package models;

import java.io.Serializable;

/**
 * Created by lacroiix on 02/10/15.
 */
public enum LocalType implements Serializable {
    PREFEITURA("Prefeitura"), CAMARA("Câmara");

    private String value;

    LocalType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getEnum(String value) {
        for(LocalType v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v.toString();
        throw new IllegalArgumentException();
    }
}
