package controllers;

import com.avaje.ebean.Ebean;
import models.MainUser;
import org.apache.commons.lang3.text.WordUtils;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import util.SecureUtil;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.PersistenceException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by lacroiix on 02/10/15.
 */
public class MainUserController extends Controller {

    static Form<MainUser> userForm = Form.form(MainUser.class);

    public Result index() {
        return redirect(routes.MainUserController.users());
    }

    public Result users() {
        return ok(views.html.userSearch.render(new ArrayList<MainUser>(), userForm));
    }

    public Result homeNewUser() {
        return ok(views.html.user.render(userForm));
    }

    public Result newUser() {
        Form<MainUser> filledForm = userForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            flashGlobalError("Oops! Falha ao criar este Usuário");
            return badRequest(
                    views.html.user.render(filledForm)
            );
        } else {
            MainUser user = filledForm.get();
            String capitalizedName = WordUtils.capitalizeFully(user.getName());
            user.setName(capitalizedName);

            try {
                //Encriptando a Senha
                user.setPassword(SecureUtil.encryptPassword(user.getPassword()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            int verifyEmail = MainUser.find.where()
                    .like("email", "%" + user.getEmail() + "%").findRowCount();

            try {
                if (verifyEmail > 0) {
                    flashGlobalError("Oops, Email já cadastrado.");
                    return badRequest(
                            views.html.user.render(filledForm)
                    );
                } else {
                    MainUser.create(user);
                }

            } catch (Exception e) {
                flashGlobalError("Oops! Falha ao criar este Usuário");
                return badRequest(
                        views.html.user.render(filledForm)
                );
            }
            return redirect(routes.MainUserController.users());
        }
    }

    public Result editUser(Long id) {
        userForm = userForm.fill(MainUser.find.byId(id));
        return ok(views.html.userEdit.render(userForm, id));
    }

    public Result update(Long id) {
        Form<MainUser> editForm = userForm.bindFromRequest();
        if (editForm.hasErrors()) {
            LocalController.flashGlobalError("Ooops.. Algo está errado. Verifique todos os campos!");
            return badRequest(views.html.userEdit.render(userForm, id));
        }

        MainUser user = editForm.get();
        user.setId(id);
        Ebean.update(user);
        userForm = userForm.fill(new MainUser());
        return redirect(routes.MainUserController.index());
    }

    public Result deleteUser(Long id) {
        try {
            MainUser.delete(id);
            return redirect(routes.MainUserController.users());
        } catch (PersistenceException pe) {
            MainUserController.flashGlobalError();
            return badRequest(
                    views.html.userSearch.render(new ArrayList<MainUser>(), userForm)
            );
        }
    }

    public Result findUserByName() {
        Form<MainUser> filledForm = userForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            return badRequest(
                    views.html.userSearch.render(new ArrayList<MainUser>(), filledForm)
            );
        } else {
            MainUser user = filledForm.get();
            List<MainUser> listUsers = MainUser.find.where().like("name", "%" + user.getName
                    () + "%").findList();
            return ok(views.html.userSearch.render(listUsers, userForm));
        }
    }

    public static void flashGlobalError() {
        flashGlobalError("Oppps... você deve remover os Locais deste cliente antes!");
    }

    public static void flashGlobalError(String msg) {
        flash("globalError", msg);
    }

}
