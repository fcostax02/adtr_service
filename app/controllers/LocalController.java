package controllers;

import com.avaje.ebean.Ebean;
import models.Local;
import models.MainUser;
import org.json.simple.JSONObject;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import util.GcmSender;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lacroiix on 02/10/15.
 */
public class LocalController extends Controller {

    static Form<Local> localForm = Form.form(Local.class);
    private List<String> androidTargets = new ArrayList<String>();

    public Result index() {
        return redirect(routes.LocalController.locals());
    }

    public Result locals() {
        return ok(views.html.local.render(Local.all(), localForm, (new MainUser()).getDefaultUser()));
    }

    public Result editLocal(Long id) {
        Local local = Local.find.byId(id);
        localForm = localForm.fill(local);
        return ok(views.html.localEdit.render(localForm, local.getMainUser(), local.getId()));
    }

    public Result localByUser(Long userId) {
        List<Local> localsByUser = Local.find.where().eq("user_id", userId).findList();
        return ok(views.html.local.render(localsByUser.isEmpty() ? new ArrayList<Local>()
                : new ArrayList<Local>(localsByUser), localForm, MainUser.find.byId(userId)));
    }

    public Result newLocal(Long userId) {
        Form<Local> filledForm = localForm.bindFromRequest();
        if (filledForm.hasErrors()) {
            List<Local> localsByUser = Local.find.where().eq("user_id", userId).findList();
            return badRequest(
                    views.html.local.render(localsByUser, filledForm, MainUser.find
                            .where().eq("id", userId).findUnique())
            );
        } else {
            Local local = filledForm.get();
            MainUser user = MainUser.find.where().eq("id", userId).findUnique();

            local.setMainUser(user);

            List<Local> locals = new ArrayList<Local>();
            locals.add(local);
            user.setLocals(locals);

            Local.create(local);
            MainUser.create(user);
            return redirect(routes.LocalController.localByUser(userId));
        }
    }

    public Result update(Long id) {
        Form<Local> editForm = localForm.bindFromRequest();
        Local localFromForm = Local.find.byId(id);
        if (editForm.hasErrors()) {
            LocalController.flashGlobalError(String.valueOf(editForm.get().getId()));
            return badRequest(views.html.localEdit.render(localForm, localFromForm.getMainUser(), id));
        }

        Local local = editForm.get();
        local.setId(id);
        Ebean.update(local);
        localForm = localForm.fill(new Local());
        return redirect(routes.LocalController.localByUser(localFromForm.getMainUser().getId()));
    }

    public Result deleteLocal(Long id, Long userId) {
        Local.delete(id);
        return redirect(routes.LocalController.localByUser(userId));
    }

    public Result homeSendKey(Long id) {
        localForm = localForm.fill(Local.find.byId(id));
        return ok(views.html.sendKey.render(localForm, id));
    }

    public Result sendKey(Long id) {
        Form<Local> filledForm = localForm.bindFromRequest();
        Local local = Local.find.byId(id);
        Local form = filledForm.get();

        local.setKey(form.getKey());

        Ebean.update(local);
        try {
            sendNotification(local);
            localForm = localForm.fill(new Local());
            return redirect(routes.LocalController.localByUser(local.getMainUser().getId()));
        } catch (Exception e) {
            return badRequest(views.html.sendKey.render(localForm, id));
        }
    }

    /**
     * Envia uma chave (String Key) para um dispositivo android.
     *
     * @param local
     */
    public void sendNotification(Local local) {
        androidTargets.add(local.getMainUser().getIdGcmUser());

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", local.getId());
        try {
            jsonObject.put("name", URLEncoder.encode(local.getName(), "UTF-8"));
            jsonObject.put("key", URLEncoder.encode(local.getKey(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        System.out.println(jsonObject.toString());

        GcmSender.send(androidTargets, jsonObject.toString());

    }

    public static void flashGlobalError(String msg) {
        flash("globalError", msg);
    }

}