package controllers;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Local;
import models.MainUser;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;

/**
 * Created by lacroiix on 29/10/15.
 */
public class WebService extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result registerGcmKey() {
        JsonNode jNode = Controller.request().body().asJson();

        MainUser user = null;
        try {
            String s = jNode.path("regId").toString();
            Long userId = jNode.path("userId").asLong();

            s = s.replace("\"", "");

            user = MainUser.find.byId(userId);

            user.setIdGcmUser(s);
            Ebean.update(user);
            return ok(Json.toJson("OK"));
        } catch (Exception e) {
            return ok(Json.toJson("FALHOU"));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result appLogin() {
        JsonNode jNode = Controller.request().body().asJson();

        String username = jNode.path("username").toString();
        String password = jNode.path("password").toString();

        username = username.replace("\"", "");
        password = password.replace("\"", "");

        MainUser user = MainUser.find.where().like("email", "%" + username + "%").findUnique();
        ObjectMapper mapper = new ObjectMapper();

        if (user != null && user.getPassword() != null) {
            try {
                if (user.getPassword().equals(password)) {
                    if (!user.getLocals().isEmpty()) {
                        return ok(Json.toJson(mapper.writeValueAsString(user)));
                    } else {
                        user.setLocals(new ArrayList<Local>());
                        return ok(Json.toJson(mapper.writeValueAsString(user)));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return badRequest(Json.toJson("Usuário ou senha incorreta"));
        } else {
            return badRequest(Json.toJson("Usuário ou senha incorreta"));
        }
    }

}
